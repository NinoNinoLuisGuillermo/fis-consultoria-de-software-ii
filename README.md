<img src="imagenes/nearsoft.png" width="200" height="200" />

# NEARSOFT

Link: https://nearsoft.com/

Ubicación: https://goo.gl/maps/YdBVCba2aMm1FGr57

Acerca de: *Somos una comunidad de más de 300 personas en oficinas en todo México, y nos reunimos dos veces al año para nuestra Semana de Team Building. Durante esa semana trabajamos para mejorar nuestra relación laboral, definiendo nuestras metas y —por supuesto— nos divertimos mucho.*

Presencia:
1. Twitter: https://twitter.com/Nearsoft
2. Facebook: https://www.facebook.com/Nearsoft/

Ofertas laborales: https://nearsoft.com/faq/

Blog de ingeniería: https://nearsoft.com/blog/

1. Tecnologías:  
- Java
- React
- Ruby
- Python

<img src="imagenes/chelada.io.png" width="200" height="200" />

# MICHELADA.IO

Sitio web: https://www.michelada.io/

Ubicación: https://goo.gl/maps/qxcHLtZCgB5g4UrE8

Acerca de: Planificamos, codificamos y lanzamos increíbles productos web y móviles

Servicios: *Desarrollamos software para soluciones interactivas y picantes. Ya sea que sea una empresa nueva o una corporación, podemos ayudarlo a transformar grandes ideas en software utilizable para productos atractivos.*

Presencia:
1. Facebook: https://www.facebook.com/michel
2. Github: https://github.com/michelada
3. Twitter: https://twitter.com/micheladaio

Blog de Ingeniería: https://www.mindhub.mx/

<img src="imagenes/icalialabs.png" width="200" height="200" />

# ICALIALABS

Sitio web: https://www.icalialabs.com/

Ubicación: https://goo.gl/maps/7RgviVkHZ7C4NW4r7

Acerca de: *En Icalia Labs transformamos negocios con soluciones digitales poderosas y adaptables que satisfacen las necesidades de hoy y desbloquean las oportunidades del mañana.*

Servicios: https://www.icalialabs.com/services/ruby-on-rails-software-development

Presencia:
1. linked in: https://www.linkedin.com/company/icalia-labs/
2. Github: https://github.com/IcaliaLabs
3. Icalia Labs: https://dribbble.com/icalialabs/members


Ofertas laborales: https://www.icalialabs.com/work-design-sprint-custom-software-legacy-software

Blog de Ingeniería: https://www.icalialabs.com/blog-software-development-tips-and-news

Tecnologías:
1. Lenguajes y Frameworks
    Ruby, Ruby on Rails, Python, ReactJS, EmberJS, GraphQL, iOS, Android, React Native, Bootstrap, HTML, CSS, JS

2. Sistemas de base de datos
    PostgreSQL, MySQL 


<img src="imagenes/tacit.png" width="200" height="200" />

# TACIT

Sitio web: https://www.tacitknowledge.com/

Ubicación: https://goo.gl/maps/fPFTTzjd42WhcEaRA

Acerca de: *El trabajo de nuestro proyecto ha abarcado una amplia gama y escala a lo largo de los años, pero todos con una causa común;
ayudar a resolver los problemas más complejos que pudiéramos encontrar*

Servicios: https://www.tacitknowledge.com/what-we-do/

Presencia:
1. Twitter; https://twitter.com/tacitknowledge

Ofertas laborales: https://www.tacitknowledge.com/our-work/

Blog de Ingeniería: https://www.tacitknowledge.com/news/

<img src="imagenes/tango.png" width="200" height="200" />

# TANGO

Sitio web: https://tango.io/


Acerca de: *Tango (anteriormente TangoSource), ayuda a empresas emergentes, medianas y empresariales innovadoras a desarrollar productos digitales impactantes a través de asociaciones apasionadas. ¡Hemos ayudado a más de 100 empresas a desarrollar y escalar sus productos de software desde 2009!*

Servicios:
1. Análisis de requisitos de producto 
2. Planificación de la cartera de productos 
3. Pronóstico de versiones 
3. Edificio MVP


Presencia:
1. Facebook: https://www.facebook.com/Tango.ioMX
2. Twitter: https://twitter.com/tango_io
3. Instagram: https://www.instagram.com/tangodotio/
4. Linkedin: https://www.linkedin.com/company/tango-io/

Ofertas laborales: https://tango.io/work

Blog de Ingeniería: https://blog.tango.io/

1. Tecnologías:}
- AngularJS
- Ruby
- HAML
- Apache 
- Boostrap


<img src="imagenes/itexico.png" width="200" height="200" />

# ITEXICO

Sitio web: https://www.itexico.com/

Ubicación: https://goo.gl/maps/VnqS5nyuQ3wnWfi58

Acerca de: *Ayudamos a nuestros clientes a conceptualizar, diseñar, construir, enviar y mantener un excelente software que responda a las necesidades cambiantes del mercado y del producto comercial.*

1. Servicios:
- Centro de prototipos
- Sprints de diseño de producto
- Diseño de producto + Soluciones de desarrollo

Presencia:
- Twitter: https://twitter.com/iTexico
- Linkedin: https://www.linkedin.com/showcase/improving-nearshore

Ofertas laborales: https://www.itexico.com/careers

Blog de Ingeniería: https://www.itexico.com/blog

Tecnologías:
- Desarrolladores de IOS
- Java
- PHP
- Python


<img src="imagenes/contpa.png" width="200" height="200" />

# CONTPA

Sitio web: https://www.contpaqi.com/productos?cuenta=Contpaqi_sitio&utm_term=contpaqi&utm_campaign=Contpaqi%20Marca%20Performance%20NAL&utm_source=google&utm_medium=cpc&hsa_acc=7211232660&hsa_cam=14937393551&hsa_grp=136965408628&hsa_ad=339036526523&hsa_src=g&hsa_tgt=aud-825072257945:kwd-10620849946&hsa_kw=contpaqi&hsa_mt=b&hsa_net=adwords&hsa_ver=3

Ubicación: https://goo.gl/maps/bYcfc2kTVnJMP6NP7

Acerca de: *En CONTPAQi® somos pioneros en software contable y administrativo. Nuestro objetivo es impulsar a las empresas y profesionales a lo largo de México*

Servicios:
- Integra y controla tu proceso contable
- Administra tu nómina 
- Administración de tus cuentas 
- Descarga CFDU del portal del SAT

Presencia:
- Facebook: https://www.facebook.com/CONTPAQi
- Linkedin: https://www.linkedin.com/company/contpaqi1
- Youtube: https://www.youtube.com/contpaqi1

Ofertas laborales: https://www.linkedin.com/company/contpaqi1

Blog de Ingeniería: https://blog.contpaqi.com/




<img src="imagenes/svitla.png" width="200" height="200" />

# SVITLA

Sitio web: https://svitla.com/


Acerca de: *Es su conducto hacia las innovaciones tecnológicas más vanguardistas, desde Web y dispositivos móviles hasta Big Data e Internet de las cosas. Entregamos un valor incomparable a nuestros clientes.*

Presencia:
- Instagram: https://www.instagram.com/svitlasystems/
- Linkedin: https://www.linkedin.com/company/svitla-systems-inc-/?trk=biz-companies-cym
- Facebook: https://www.facebook.com/SvitlaSystems

Ofertas laborales: https://svitla.com/career

Blog de Ingeniería: https://svitla.com/blog

Tecnologías:
- Amazon web service
- E-commerce
- Big data
- SEO
- Mobile Solutions


<img src="imagenes/magmalabs.png" width="200" height="200" />

# MAGMALABS

Sitio web: https://www.magmalabs.io/

Ubicación: https://goo.gl/maps/Z1RRbQic32CtHA7Q9

Acerca de: *MagmaLabs es el hogar de los desarrolladores de software más impactantes de América Latina*

Servicios:
- Consultoría de productos ajustados
- Diseño UX / UI para web y móvil
- Desarrollo de proyectos de software de ciclo de vida completo

Presencia:
- Facebook:https://www.facebook.com/magmalabsio
- Twitter: https://twitter.com/weareMagmaLabs
- Linkedin: https://www.linkedin.com/company/magmalabs

Ofertas laborales: https://magmalabs.bamboohr.com/jobs/

Blog de Ingeniería: http://blog.magmalabs.io/

Tecnologías:
- Ruby
- Solidus
- React 

<img src="imagenes/proscai.png" width="200" height="200" />

# PROSCAI

Sitio web: https://www.proscai.com/?k=proscai&c=1003582446&ag=51128370913

Ubicación: https://goo.gl/maps/KGmXZbSAFbdjUtzC8

Acerca de: *Nuestra empresa nace dentro la industria con el propósito de desarrollar aplicaciones de tecnología que faciliten el trabajo de las personas, logrando que las empresas lleguen a su máximo potencial.*

Servicios: 
- Desarrollo 
- Implementación
- Capacitación
- Consultoría

Ofertas laborales: https://www.proscai.com/careers

Blog de Ingeniería: https://blog.proscai.com/

Tecnologías:
- Galio
- Redi
- Tienda virtual 


<img src="imagenes/mobiik.png" width="200" height="200" />

# MOBIIK

Sitio web: https://www.mobiik.com/

Ubicación: https://goo.gl/maps/wjyfkfH8SqZyRix1A

Acerca de: *Mobiik proporciona talento altamente calificado, al mismo tiempo que ofrece soluciones tecnológicas para ayudar a nuestros clientes a hacer crecer su negocio y mejorar sus operaciones* 

Servicios:
- Desarrollo de aplicaciones 
- Servicios en la nube
- Datos e IA 

Tecnologías:
- Integración DevOps
- RedHat OpenShift
- Azure 
- Amazon Elastic Kubernetes

<img src="imagenes/headspring.png" width="200" height="200" />
 
 # HEADSPRING

Sitio web: https://headspring.com/

Ubicación: https://goo.gl/maps/JLBCZvgCiUVhLEWu9

Acerca de: *Nos asociamos con líderes de TI para ofrecer soluciones de estrategia, tecnología y transformaciónque mejoran la agilidad y generan valor comercial duradero.*

Servicios:
- Arquitectura
- Cloud optimization
- Code review 
- Mainframe modernization

Presencia:
- Facebook: https://www.facebook.com/HeadspringTeam/
- Twitter: https://twitter.com/headspring

Ofertas laborales: https://headspring.com/about/careers/

Blog de Ingeniería: https://headspring.com/insights/our-blog/


<img src="imagenes/telepro.png" width="200" height="200" />

# TELEPRO

Sitio web: https://telepro.com.mx/

Ubicación: https://goo.gl/maps/HiUoc6wkytqu4xQg8

Acerca de: *empresa 100% mexicana, con 25 años de presencia en el mercado de tecnología de información. Nuestros socios y colaboradores, cuentan con amplia experiencia en el sector financiero y tecnológico.v*

Servicios:
- Administracion ciclo de credito 
- Business Intelligence 
- Analytical Solutions

Presencia: 
- Linkedin: https://www.linkedin.com/company/servicios-telepro-s.a.-de-c.v./
- Faceboook: https://www.facebook.com/InfoTelepro

Ofertas laborales: https://telepro.com.mx/desarrollador-jr.html



<img src="imagenes/descarga.png" width="200" height="200" />

# GFT MEXICO

Sitio web: https://www.gft.com/mx/es/index/

Ubicación: https://goo.gl/maps/dQroUfMZXhUrh8D2A

Acerca de: *Comenzamos como una pequeña empresa en la Selva Negra alemana y hemos crecido hasta convertirnos en especialistas del sector financiero a nivel internacional. Somos pioneros en ofrecer servicios nearshore desde 2001 y contamos con un equipo de 7.000 colaboradores en más de 15 países.*


Presencia:
- Facebook : https://www.facebook.com/gft.mex
- Instagram :  https://www.instagram.com/gft_tech/
- Twitter: https://twitter.com/gft_mx

Ofertas laborales: https://jobs.gft.com/Mexico/go/mexico/4412401/

Tecnologías:
- Greencoding
- Analisis de datos
- IA


<img src="imagenes/ intelimétrica.png" width="200" height="200" />

# INTELIMÉTRICA

Sitio web: https://www.intelimetrica.com/

Ubicación: https://goo.gl/maps/Dnq1XW9VxS3bcaj17

Acerca de: *Ayudamos a las organizaciones a acelerar la innovación y el crecimiento empresarial mediante tecnología, ingeniería y diseño superiores.*

Servicios:
- IA
- Ml
- Ingenieria de software
- Diseño UX

Presencia: 
- Twitter: https://twitter.com/Intelimetrica
- Facebook: https://www.facebook.com/intelimetrica
- Github: https://github.com/Intelimetrica


Ofertas laborales: https://www.intelimetrica.com/careers


Tecnologías:
- DevOps
- Back-end
- Front-end


